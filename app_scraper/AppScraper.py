import re
import requests
import bs4


class AppScraper:
    def __init__(self, url):
        self.name = ''
        self.version = ''
        self.changelog = ''
        self.release_date = ''
        self.scrap(url)

    def scrap(self, url):
        response = requests.get(url)
        page = bs4.BeautifulSoup(response.text, "html.parser")
        self._scrap_name(page)
        self._scrap_date(page)
        self._scrap_version(page)
        self._scrap_changelog(page)

    def _scrap_name(self, page):
        try:
            self.name = page.find(id='btAsinTitle').get_text()
        except AttributeError:
            pass
    
    def _scrap_date(self, page):
        release_date = ''
        try:
            release_date = page.select('#productDetailsTable li')[1].get_text()
            release_date = re.search(r': (?P<date>\w.*)', release_date).group('date')
        except (AttributeError, IndexError):
            pass
        self.release_date = release_date
        
    def _scrap_version(self, page):
        version = ''
        try:
            version = page.select('div.bucket div.content strong')[0].get_text()
            version = re.search(r' (?P<version>[\d+.]+)', version).group('version')
        except (AttributeError, IndexError):
            version = 'Varies by device'
        self.version = version

    def _scrap_changelog(self, page):
        changelog = ''
        try:
            changelog = page.select('div.bucket div.content ul')[0].get_text()
            changelog = changelog[1:]
            changelog = changelog.replace('\n', '<br />')
        except (AttributeError, IndexError):
             pass
        self.changelog = changelog
