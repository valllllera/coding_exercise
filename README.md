Sample user flow
================

The user flow should be the following:

1. I access the home page where I can paste an Appâ€™s Amazon Store URL into a text box and submit it. For example, the URL to the Facebook App on the Amazon Store is http://www.amazon.com/Facebook/dp/B0094BB4TW

2. After submitting the URL, the website returns a page displaying the following information about the App:

* App name
* App version
* Changelog for the current version
* Release date 

How to run the project
================

Simply run:
```
python manage.py runserver
```