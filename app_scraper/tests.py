# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

class TestCalls(TestCase):

    def setUp(self):
        pass

    def test_search_view(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'search.html')
