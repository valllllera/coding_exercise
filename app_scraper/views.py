# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import TemplateView
from django.http import JsonResponse, Http404
from rest_framework.views import APIView

from . import AppScraper as AS


class SearchView(TemplateView):
    template_name = "search.html"


class SearchResults(APIView):
    def get(self, request, format=None):
        url = ''
        try:
            url = request.GET["url"]
        except KeyError:
            raise Http404("Wrong URL")

        app = AS.AppScraper(url)

        if app.name == '':
            raise Http404("Wrong URL")

        return JsonResponse(dict(app.__dict__))
