$(document).ready(function(){
   });

$( "#search-btn" ).click(function() {
    $("#data").hide();
    $("#progress .progress-bar").show();
    
    url = $('#input-app-url').val()

    $.getJSON( "/api/scrap?url="+url).done(function( data ) {    
        name = data['name'];
        version = data['version'];
        changelog = data['changelog'];
        release_date = data['release_date'];

        $("#app-name").html(name);
        $("#app-version").html(version);
        $("#app-changelog").html(changelog);
        $("#app-release-date").html(release_date);

        $("#progress .progress-bar").hide();
        $("#data").fadeIn( "slow" );

      }).fail(function(jqxhr){
        $("#data").hide();
        $("#progress .progress-bar").hide();
        alert('Something went wrong, try to enter the full URL')
     });
});