from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.SearchView.as_view(), name='search'),
    url(r'^api/scrap$', views.SearchResults.as_view(), name='results'),
]